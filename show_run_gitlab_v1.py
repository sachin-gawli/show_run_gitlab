import os
from datetime import datetime
import socket
import gitlab

from nornir import InitNornir
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result

from nornir_napalm.plugins.tasks import napalm_get

project_id = "Enter your Gitlab Project Number"
api_token = "Enter your API token for above project"

config_folder = os.path.dirname(__file__) + "/configs"


def ssh_check(host):
    """
    This Fuction checks the SSH Port 22 availablity on the
    host. If device accepts SSH port, it returns True else
    returns False.
    Args:
        host ([str]): [IP Address of the Device]
    """
    # Initialse Socket Object for IPv4 Address Family
    port = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Set timeout if SSH Port is not open.
    port.settimeout(2)
    try:
        # Open SSH Conection to the device
        port.connect((host, 22))
        return True
    except:
        return False


def gitlab_update(project_id, api_token, cfg_folder):
    """
    This fuction is used Create/Update files directly on GitLAB
    project repository. This fuction takes three arguments.
    Args:
        project_id ([int]): Gitlab Project ID
        token ([str]): Authorization Token
        cfg_folder ([str]): Local Conffiguraion Folder
    """
    # Initialise Gitlab Object

    gl = gitlab.Gitlab("https://gitlab.com/", private_token=api_token)

    # Initialse Project Object using API Key Authorization

    try:
        project = gl.projects.get(project_id)
        print("=" * 60)
        print(f"Sucessfully Logged into Gitlab Project: {project.name}" + "\n")
    except gitlab.exceptions.GitlabAuthenticationError:
        print("Please Check the API Authentcation Token: Unauthorised/Access Denied")

    # Get the list of .cfg Files from Remote Repo.

    print(f"Getting list of files from Remote Repo: {project.name}")
    remote_files_tree = [
        file["name"]
        for file in project.repository_tree()
        if file["name"].endswith("cfg")
    ]
    # print(remote_files_tree)

    # Get the list of .cfg Files from local config folder

    print(f"Getting list of files from local {cfg_folder} Folder")
    local_files_tree = [file for file in os.listdir(cfg_folder) if file.endswith("cfg")]

    # print(local_files_tree)

    # Apply Set theory to get the list of files which are already present on remote Repo.
    # This Set will be used to generate update action

    common_files = set(local_files_tree) & set(remote_files_tree)

    # Apply Set theory to get the list of files which are only present locally & needs to
    # be added newly to remote repo. This set will be used to generate create action

    new_files = set(local_files_tree) - set(remote_files_tree)

    actions_update = []

    # For loop for common files which needs update action

    for file in common_files:
        with open(os.path.join(cfg_folder, file)) as local_file:
            local_file_content = local_file.read()
        remote_file_content = project.files.raw(file_path=file, ref="main").decode()
        if local_file_content != remote_file_content:
            file_action = {"action": "update", "file_path": file}
            file_action["content"] = open(os.path.join(cfg_folder, file)).read()
            actions_update.append(file_action)

    # For loop for New files which needs create action

    for file in new_files:
        file_action = {"action": "create", "file_path": file}
        file_action["content"] = open(os.path.join(cfg_folder, file)).read()
        actions_update.append(file_action)

    # print(actions_update)

    # Format Data for final commit to Gitlab Project

    data = {
        "branch": project.default_branch,
        "commit_message": "Pythonic Commit",
        "author_email": "sachin.gawli@yahoo.com",
        "author_name": "Sachin Gawli",
        "actions": actions_update,
    }
    # print(data)

    print("Executing Commit Action on Remote Repository\n")
    if actions_update:
        try:
            commit = project.commits.create(data)
            print(f"Sucessfully updated configuration changes on remote Repo.\n")
        except:
            print("Sorry, Something went wrong !!!\n")
    else:
        print("There is nothing to update !!\n")


def create_config_folder():
    """
    This Function creates "Configs" folder under current working
    directory if it does not exists already
    """
    if not os.path.exists(config_folder):
        print("Creating Config folder in the current working directory\n")
        os.makedirs(config_folder)
    else:
        print("Config folder already exists\n")


def save_config_file(host, content):
    """
    This fuction save the configuration to file with
    current timestamp.
    Args:
        host ([str]): [hostname of the device]
        content ([str]): [show run configuration string]
    """
    cfg_file_name = f"{host}_show_run.cfg"
    print(f"Saving Configuration for the {host} Device")
    with open(os.path.join(config_folder, cfg_file_name), "w") as file:
        file.write(content)


def get_show_run(task):
    """
    This group of task first checks if host is reachable over SSH.
    Then it will collect running configuration using NAPALM &
    saves this configuration to file under backup folder
    """
    ssh_status = ssh_check(task.host.hostname)
    if ssh_status:
        print(f"Collecting running configuration from the {task.host} device")
        output = task.run(task=napalm_get, getters=["config"])
    else:
        print(f"{task.host} is not Reachable")
    cfg = output.result["config"]["running"]
    save_config_file(task.host, cfg)


def clear_config_folder(cfg_folder, file_extension="cfg"):
    """
    This fuction removes all the files from specified folder
    with file extesion provided as a argument. Once all the
    files has been removed, this fuction will remove the empty
    directory.
    Args:
        cfg_folder ([str]): [config folder name]
        file_extension (str, optional): [File extesion ex: txt, cfg etc..]. Defaults to "cfg".
    """
    # Get the list of .cfg files from local config folder
    local_cfg_dir = f"{os.path.dirname(__file__)}/{cfg_folder}"
    local_files_tree = [
        file for file in os.listdir(local_cfg_dir) if file.endswith(file_extension)
    ]
    for cfg_file in local_files_tree:
        try:
            file_name = f"{os.path.dirname(__file__)}/configs/{cfg_file}"
            print(f"Removing {cfg_file} file")
            os.remove(file_name)
        except OSError as err:
            print("Error: %s - %s." % (err.filename, err.strerror))
            print(
                f"The File {cfg_file} can not be removed. Please delete this file manually"
            )
    print("Cleaned Up all cfg files")
    print(f"Removing Empty {local_cfg_dir} directory")
    os.rmdir(local_cfg_dir)


if __name__ == "__main__":
    nr = InitNornir("./01_Projects/config.yml")
    nr_swithes = nr.filter(F(tags__contains="local"))
    create_config_folder()
    nr_swithes.run(task=get_show_run)
    gitlab_update(project_id, api_token, config_folder)
    clear_config_folder("configs", "cfg")
