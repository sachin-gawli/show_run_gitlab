The main objective of this script is to use version control for
network devices configurations using Gitlab.

This script is based on "nornir" framework to gather network
device configurations using NAPALM module & then we are using
"gitlab" python library to push configurations from local
folder to remote gitlab project(repository).

We had incorporated basic logic to avoid file commits if
there are no changes as well as logic to create new files
directly on the remote project repository if that file is
not present.

Note:

Add correct Project ID & API Token from Gitlab under script
veriables before using.

Tested with python Version 3.8+

Required Packages using python venv:

- pip3 install nornir
- pip3 install nornir_napalm
- pip3 install python-gitlab

Happy Coding !!!!
